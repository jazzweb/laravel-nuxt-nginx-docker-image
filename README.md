# Nginx docker container for nuxt + laravel application
Based on [nginx:1.17.8-alpine](https://github.com/nginxinc/docker-nginx/blob/5971de30c487356d5d2a2e1a79e02b2612f9a72f/mainline/alpine/Dockerfile)

## Build instruction
`docker build --build-arg DOMAIN=YOUR_DOMAIN .`

### Possible build arguments
- `DOMAIN` - domain of nuxt application (required)
- `NUXT_CONTAINER` - name of nuxt container (default `nuxt`)
- `NUXT_PORT` - nuxt container exposed port (default `3000`)
- `LARAVEL_DOMAIN` - domain of laravel application (default `laravel.DOMAIN`)
- `LARAVEL_ROOT_PATH` - nginx root path to laravel installation folder, not for public (default `/var/www/laravel`)
- `LARAVEL_CONTAINER` - name of laravel container (default `laravel`)
- `LARAVEL_PORT` - laravel container exposed port (default `9000`)

## How to use
#### Manual:
- clone or fork
- build docker

#### GitLab CI:
- clone or fork
- rename .gitlab-ci.yml.example to .gitlab-ci.yml `mv .gitlab-ci.yml.example .gitlab-ci.yml`
- change registry path image in .gitlab-ci.yml from `jazzweb/laravel-nuxt-nginx-docker-image` to yours
- add variables to your project CI/CD settings

