FROM nginx:1.17.8-alpine

ARG DOMAIN
ARG NUXT_CONTAINER=nuxt
ARG NUXT_PORT=3000
ARG LARAVEL_DOMAIN=laravel.$DOMAIN
ARG LARAVEL_ROOT_PATH="/var/www/laravel"
ARG LARAVEL_CONTAINER=laravel
ARG LARAVEL_PORT=9000

# Copy configs to nginx conf.d folder
COPY sites/* /etc/nginx/conf.d/

# Set arguments to nuxt conf
RUN sed -i "s/YOUR_DOMAIN/$DOMAIN/g;s/NUXT_CONTAINER/$NUXT_CONTAINER/g;s/NUXT_PORT/$NUXT_PORT/g" /etc/nginx/conf.d/nuxt.conf

# Set arguments to laravel conf
RUN sed -i "s/LARAVEL_DOMAIN/$LARAVEL_DOMAIN/g;s#LARAVEL_ROOT_PATH#$LARAVEL_ROOT_PATH/public#g;s/LARAVEL_CONTAINER/$LARAVEL_CONTAINER/g;s/LARAVEL_PORT/$LARAVEL_PORT/g" /etc/nginx/conf.d/laravel.conf
